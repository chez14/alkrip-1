import React, { useState, useEffect } from 'react';
import { Container, Row, Col, FormGroup, Input, Label } from 'reactstrap';

import { Jose } from 'jose-jwe-jws'
import { QRCode } from 'react-qrcode'

const daftarProvinsi = [
  {
    "name": "Jawa Barat",
    "kota": [
      {
        name: "Bandung",
        kecamatan: [
          {
            name: "Ciumbuleuit"
          },
          {
            name: "Kopo"
          },
        ]
      }
    ]
  }
]

var ec_key = {
  "kty": "EC",
  "crv": "P-256",
  "x": "f83OJ3D2xF1Bg8vub9tLe1gHMzV76e8Tus9uPHvRVEU",
  "y": "x_FEzRu9m36HLN_tue659LNpXW6pCyStikYjKIWI5a0",
  "d": "jpsQnnGQmL-YBIffH1136cspYG6-0iY7X1fCE9-E9LI"
}
var cryptographer = new Jose.WebCryptographer();
cryptographer.setContentSignAlgorithm("ES256");

function App() {
  const [provinsi, setProvinsi] = useState(0);
  const [kota, setKota] = useState(0);
  const [kecamatan, setKecamatan] = useState(1);
  const [postalcode, setPostalcode] = useState(14045);

  const [qr, setQr] = useState("");

  useEffect(() => {
    //generate QRCode
    var signer = new Jose.JoseJWS.Signer(cryptographer);
    var date = new Date();
    date.setDate(date.getDate() + 14);
    signer.addSigner(ec_key).then(function () {
      signer.sign({
        P: provinsi,
        C: kota,
        K: kecamatan,
        postalCode: postalcode,
        iss: "https://tokohijau.tld/",
        jti: 1532,
        exp: date.getTime()/1000
      }, null, {}).then(message => {
        console.log(message.CompactSerialize());
        setQr(message.CompactSerialize());
      })
    })

    return () => {
    };
  }, [provinsi, kota, kecamatan, postalcode])
  return (
    <Container>
      <Row className="align-items-center" style={{ height: "100vh" }}>
        <Col xs={12}>
          <Row>
            <Col xs={12}>
              <div className="my-5">
                <h3>Seller: Print Label</h3>
              </div>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <FormGroup>
                <Label for="provinsi">Provinsi</Label>
                <Input type="select" name="select" id="provinsi" onChange={event => setProvinsi(event.currentTarget.value)}>
                  {daftarProvinsi.map(el => <option key={el.name}>{el.name}</option>)}
                </Input>
              </FormGroup>
              <FormGroup>
                <Label for="kota">Kota</Label>
                <Input type="select" name="select" id="kota" onChange={event => setKota(event.currentTarget.value)}>
                  {daftarProvinsi[provinsi].kota.map(el => <option key={el.name}>{el.name}</option>)}
                </Input>
              </FormGroup>
              <FormGroup>
                <Label for="kecamatan">Kecamatan</Label>
                <Input type="select" name="select" id="kecamatan" onChange={event => setKecamatan(event.currentTarget.value)} defaultValue={kecamatan}>
                  {daftarProvinsi[provinsi].kota[kota].kecamatan.map((el) => <option key={el.name}>{el.name}</option>)}
                </Input>
              </FormGroup>
              <FormGroup>
                <Label for="postalcode">Postal Code</Label>
                <Input type="text" name="postalcode" id="postalcode" onChange={event => setPostalcode(event.currentTarget.value)} defaultValue={postalcode} />
              </FormGroup>
            </Col>
            <Col md={4}>
              <QRCode value={qr} className="img-fluid w-100" />
            </Col>

          </Row>
        </Col>
      </Row>
    </Container>
  );
}

export default App;
